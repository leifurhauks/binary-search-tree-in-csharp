I decided to learn a bit of C# by writing a binary search tree. If you see
something that could be improved, I would love to hear about it!

It's based on Chapter 12 of Cormen et al, *Introduction to Algorithms*, 3rd edition.

Not intended to be particularly useful in real applications; I'm just writing it as an exercise. (In fact, for most applications there are better kinds of trees than a generic BST like this one. Read up on red-black trees or b-trees if you're interested.)

I may decide to use it to implement a set or map later.

I'm using MonoDevelop on Linux with NUnit 2.6.3

(Still a work in progress!)
