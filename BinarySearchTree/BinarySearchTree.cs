﻿using System;
using System.Collections.Generic;

namespace BinarySearchTree
{
	public class BSTNode<T> where T : IComparable<T>, IEquatable<T>
	{
		public BSTNode<T> parent;
		public BSTNode<T> left;
		public BSTNode<T> right;
		public T key;

		public BSTNode (BSTNode<T> parent, BSTNode<T> left, BSTNode<T> right, T key)
		{
			this.parent = parent;
			this.left = left;
			this.right = right;
			this.key = key;
		}

	}

	public class BinarySearchTree<T> where T : IComparable<T>, IEquatable<T>
	{
		private BSTNode<T> root;

		public BinarySearchTree()
		{
			root = null;
		}

		public BSTNode<T> Root
		{
			get
			{
				return root;
			}
		}

		public void insert(T value)
		{
			BSTNode<T> y = null;
			BSTNode<T> x = this.root;
			BSTNode<T> v = new BSTNode<T> (null, null, null, value);

			while (x != null)
			{
				y = x;
				if (v.key.CompareTo(x.key) < 0)
				{
					x = x.left;
				}
				else
				{
					x = x.right;
				}
			}

			v.parent = y;

			if (y == null)
			{
				this.root = v;
			}
			else if (v.key.CompareTo(y.key) < 0)
			{
				y.left = v;
			}
			else
			{
				y.right = v;
			}
		}

		public BSTNode<T> search(T query)
		{
			BSTNode<T> current = this.root;

			while (current != null && !query.Equals(current.key))
			{
				if (query.CompareTo (current.key) < 0)
					current = current.left;
				else
					current = current.right;
			}
			return current;
		}

		public void delete(T value)
		{
			BSTNode<T> z = this.search (value);

			if (z.left == null)
				transplant (z, z.right);
			else if (z.right == null)
				transplant (z, z.left);
			else
			{
				BSTNode<T> y = min (z.right);
				if (!y.parent.Equals(z))
				{
					transplant (y, y.right);
					y.right = z.right;
					y.right.parent = y;
				}
				transplant (z, y);
				y.left = z.left;
				y.left.parent = y;
			}
		}

		public BSTNode<T> min()
		{
			return min (root);
		}

		public BSTNode<T> max()
		{
			return max (root);
		}

		public T[] toArray()
		{
			List<T> list = inOrderWalkToList (this.root, new List<T> ());
			return list.ToArray ();
		}

		public static BSTNode<T> successor (BSTNode<T> node)
		{
			if (node.right != null)
				return min (node.right);

			BSTNode<T> y = node.parent;
			while (y != null && node.Equals(y.right))
			{
				node = y;
				y = y.parent;
			}
			return y;
		}

		public static BSTNode<T> predecessor(BSTNode<T> node)
		{
			if (node.left != null)
				return max (node.left);

			BSTNode<T> y = node.parent;
			while (y != null && node.Equals(y.left))
			{
				node = y;
				y = y.parent;
			}
			return y;
		}

		List<T> inOrderWalkToList(BSTNode<T> node, List<T> dest)
		{
			if (node != null)
			{
				inOrderWalkToList (node.left, dest);
				dest.Add (node.key);
				inOrderWalkToList (node.right, dest);
			}
			return dest;
		}

		static BSTNode<T> max(BSTNode<T> node)
		{
			if (node == null)
				return null;

			while (node.right != null)
			{
				node = node.right;
			}
			return node;
		}

		static BSTNode<T> min(BSTNode<T> node)
		{
			if (node == null)
				return null;

			while (node.left != null)
			{
				node = node.left;
			}
			return node;
		}

		private void transplant(BSTNode<T> u, BSTNode<T> v)
		{
			if (u.parent == null)
				this.root = v;
			else if (u.Equals (u.parent.left))
				u.parent.left = v;
			else
				u.parent.right = v;

			if (v != null)
				v.parent = u.parent;
		}
	}
}