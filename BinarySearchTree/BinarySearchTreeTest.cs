﻿using NUnit.Framework;
using System;
using BinarySearchTree;

namespace BinarySearchTreeTests
{
	[TestFixture]
	public class Test
	{
		BinarySearchTree<int> initiallyEmpty;
		BinarySearchTree<int> tree;
		readonly int[] values = {1, 2, 4, 5, 7, 8, 9, 11, 12, 13, 14, 17, 18, 20};

		[SetUp]
		public void Init()
		{
			initiallyEmpty = new BinarySearchTree<int> ();
			tree = new BinarySearchTree<int> ();

			int[] shuffledValues = getShuffledValues ();
			foreach (int i in shuffledValues)
			{
				tree.insert (i);
			}
		}

		[Test]
		public void TestInsertPreservesBSTProperty ()
		{
			int[] shuffledValues = getShuffledValues ();
			foreach (int i in shuffledValues)
			{
				initiallyEmpty.insert (i);
				Assert.NotNull (tree.search (i));
				Assert.True (verifyBSTProperty (initiallyEmpty));
			}
		}

		[Test]
		public void TestSearchForNonexistentKeyReturnsNull()
		{
			Assert.IsNull (tree.search (arrayMax (values) + 1));
		}

		[Test]
		public void TestSearchForExistingKeyReturnsNodeWithThatKey()
		{
			Random random = new Random ();
			int index = random.Next (values.Length);
			BSTNode<int> node = tree.search (values [index]);
			Assert.AreEqual (values [index], node.key);
		}

		[Test]
		public void TestMinCalledOnEmptyTreeReturnsNull()
		{
			Assert.IsNull (initiallyEmpty.min ());
		}

		[Test]
		public void TestMinOnNonEmptyTree()
		{
			Assert.AreEqual (arrayMin (values), (tree.min ()).key);
		}

		[Test]
		public void TestMaxCalledOnEmptyTreeReturnsNull()
		{
			Assert.IsNull (initiallyEmpty.max ());
		}

		[Test]
		public void TestMaxOnNonEmptyTree()
		{
			Assert.AreEqual (arrayMax (values), (tree.max ()).key);
		}

		[Test]
		public void TestToArray()
		{
			int[] array = tree.toArray();
			for (int i = 0; i < values.Length; i++)
			{
				Assert.AreEqual (values [i], array [i]);
			}
		}

		[Test]
		public void TestSuccessorReturnsNullForLargestKey()
		{
			var node = tree.max ();
			Assert.IsNull (BinarySearchTree<int>.successor (node));
		}

		[Test]
		public void TestSuccessorWithNotLargestKey()
		{
			int i = (new Random ()).Next (values.Length - 1);
			BSTNode<int> node = tree.search (values [i]);
			BSTNode<int> expectedSuccessor = tree.search (values [i + 1]);
			Assert.AreSame (expectedSuccessor, BinarySearchTree<int>.successor(node));
		}

		[Test]
		public void TestPredecessorReturnsNullForSmallestKey()
		{
			var node = tree.min ();
			Assert.IsNull (BinarySearchTree<int>.predecessor (node));
		}

		[Test]
		public void TestPredecessorWithNotSmallestKey()
		{
			int i = (new Random ()).Next (1, values.Length);
			BSTNode<int> node = tree.search (values [i]);
			BSTNode<int> expectedPredecessor = tree.search (values [i - 1]);
			Assert.AreSame (expectedPredecessor, BinarySearchTree<int>.predecessor (node));
		}

		[Test]
		public void TestDeletePreservesBSTProperty()
		{
			int[] shuffledValues = getShuffledValues ();
			foreach (int i in shuffledValues)
			{
				tree.delete (i);
				Assert.IsNull (tree.search (i));
				Assert.True (verifyBSTProperty (tree));
			}
		}

		private bool verifyBSTProperty(BinarySearchTree<int> pTree)
		{
			return verifyBSTPropertyR(pTree.Root, int.MinValue, int.MaxValue);
		}

		private bool verifyBSTPropertyR(BSTNode<int> node, int min, int max)
		{
			if (node == null)
				return true;
			if (node.key < min || node.key > max)
				return false;

			return verifyBSTPropertyR (node.left, min, node.key) && verifyBSTPropertyR (node.right, node.key, max);
		}

		private int[] getShuffledValues ()
		{
			int[] result = new int[values.Length];

			int[] copy = new int[values.Length];
			values.CopyTo (copy, 0);

			Random random = new Random ();

			for (int i = 0; i < result.Length; i++)
			{
				int index = random.Next (result.Length - i);
				result [i] = copy [index];
				if (index < result.Length - i - 1)
					copy [index] = copy [result.Length - i - 1];
			}
			return result;
		}

		private int arrayMax(int[] array)
		{
			int max = array [0];
			foreach (int i in array)
			{
				if (i > max)
					max = i;
			}
			return max;
		}

		private int arrayMin(int[] array)
		{
			int min = array [0];
			foreach (int i in array)
			{
				if (i < min)
					min = i;
			}
			return min;
		}
	}
}

